package br.com.spring.loja.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/home")
	public String index() {
		System.out.println("Bom dia!");
		return "hello-world";
	}
	@RequestMapping("/home/cadastro")
	public String cadastro() {
		System.out.println("Cadastro");
		return "hello-world";
	}
}
